<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define( 'DB_NAME', 'stendhal' );

/** Tu nombre de usuario de MySQL */
define( 'DB_USER', 'root' );

/** Tu contraseña de MySQL */
define( 'DB_PASSWORD', 'Asura123' );

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define( 'DB_HOST', 'localhost' );

/** Codificación de caracteres para la base de datos. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'VK/gGv0?pzRq|kkdI^WwU`>..E v= -0T;jE|#5SG<,ixlZU4r`~^Ihgq=GLb90N' );
define( 'SECURE_AUTH_KEY', '<x11:EsRf*PR.=cSU%]GvXdDJ5d<6:+m<cG~)9gfww/_>W|u=3BX#-^}:~NKo%C#' );
define( 'LOGGED_IN_KEY', 'd~]VieEC6d81v.+nefx~FjyK0U$r;SZ1L1G4:CC.Kl9@ox8F`eg@6m@AKMrM6Y`#' );
define( 'NONCE_KEY', 'vt:6?7iS9chphH[^ 0;3yOjl>Gk,Az#<V~60]3P06OBzHZfFPbi*;t=C8aB;DRVh' );
define( 'AUTH_SALT', 'M{`ZFklw4^cyf*Q3*F*{b6,s~KNcU5n-?,,Ou/@Q;qN,N!@%U11o|@>>mRlzN>gg' );
define( 'SECURE_AUTH_SALT', 'Y2{ib/oo! oGBa OpcGU-oH7w9v2Dhl/P^TL~GTA7l-Za[m(/KBS8}:I19:EBNn#' );
define( 'LOGGED_IN_SALT', 'I6AO1E;~F/dOsXO/[WXzt;^q.84/oN49.R,Ri]D<tZ-N}az*H%:|lz@pg^;3+tUY' );
define( 'NONCE_SALT', 'DB5d=TZ_IpSfX1Jt}3ktxvVq]0?cr1a{`qxeD<7+;ui[@z>ojelMQX6P$G:y~(B9' );

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

